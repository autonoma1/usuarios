from project import db
from project.users.models import User
from project._tests.base import BaseTestCase
from project.users._tests.utils import create_user


class TestUpdate(BaseTestCase):
    def test_update(self):
        user = create_user()

        db.session.add(user)
        db.session.commit()

        data = {
            "name": "Actualizado",
            "email": "b@b.cl",
            "password": "12356"
        }

        self.assertEquals(1, User.query.count())
        with self.client:
            response = self.client.put(
                f'/users/{user.id}',
                json=data
            )
        self.assertStatus(response, 200)
        self.assertEquals(1, User.query.count())
        json_response = response.json
        self.assertEquals(json_response['name'], data['name'])
        self.assertEquals(json_response['email'], data['email'])
        updated_user = User.query.filter_by(id=user.id).first()
        self.assertEquals(updated_user.name, data['name'])
        self.assertEquals(updated_user.email, data['email'])

    def test_update_unexisting_user(self):
        data = {
            "name": "Actualizado",
            "email": "b@b.cl",
            "password": "123435"
        }

        self.assertEquals(0, User.query.count())
        with self.client:
            response = self.client.put(
                '/users/1',
                json=data
            )
        self.assertStatus(response, 404)
        self.assertEquals(0, User.query.count())
