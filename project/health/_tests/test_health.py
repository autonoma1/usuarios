from project._tests.base import BaseTestCase


class TestCreate(BaseTestCase):
    def _call_health(self):
        with self.client:
            return self.client.get(
                '/health'
            )

    def test_health(self):
        response = self._call_health()
        self.assertStatus(response, 200)
