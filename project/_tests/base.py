from flask_testing import TestCase
from flask import current_app
from project import db


class BaseTestCase(TestCase):
    def create_app(self):
        current_app.config.from_object('project.configs.TestingConfig')
        return current_app

    def setUp(self):
        db.create_all()
        db.session.commit()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
