class BaseConfig:
    SQLALCHEMY_DATABASE_URI = 'postgres://{}:{}@{}:{}/{}'.format(
        'postgres',
        'postgres',
        'usuarios-db',
        '5432',
        'users'
    )
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECRET = '987654321'


class DevelopmentConfig(BaseConfig):
    SQLALCHEMY_ECHO = True


class ProductionConfig(BaseConfig):
    SQLALCHEMY_DATABASE_URI = 'postgres://{}:{}@{}:{}/{}'.format(
        'postgres',
        '12345678',
        'database-1.c6ilibukl1i1.us-east-2.rds.amazonaws.com',
        '5432',
        'users'
    )
    SQLALCHEMY_ECHO = False


class TestingConfig(BaseConfig):
    SQLALCHEMY_ECHO = False
