"""empty message

Revision ID: 578d34d685cf
Revises: 8bd9cc8a0af4
Create Date: 2020-09-21 21:23:20.858854

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '578d34d685cf'
down_revision = '8bd9cc8a0af4'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('address',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('street', sa.String(), nullable=False),
    sa.Column('user_id', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('address')
    # ### end Alembic commands ###
